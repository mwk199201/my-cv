export type Portfolio = {
  id: number;
  header1: string;
  subHeader1: string;
  points1: Point[];
  header2: string;
  subHeader2: string;
  points2: Point[];
  header3?: string;
  subHeader3?: string;
  points3?: Point[];
  header4?: string;
  subHeader4?: string;
  points4?: Point[];
};

type Point = {
  id: number;
  content: string;
};
