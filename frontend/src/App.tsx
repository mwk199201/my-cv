import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Main from "./Pages/Main";
import CVHeader from "./components/Header";
import { Portfolio } from "./type";

function App() {
  const portfolio: Portfolio = {
    id: 1,
    header1: "Personal summary",
    subHeader1: "profile",
    points1: [
      { id: 1, content: "content" },
      { id: 2, content: "content" },
    ],
    header2: "",
    subHeader2: "profile",
    points2: [
      { id: 1, content: "content" },
      { id: 2, content: "content" },
    ],
  };

  return (
    <div className="App">
      <CVHeader />
      <Main {...portfolio} />
    </div>
  );
}

export default App;
