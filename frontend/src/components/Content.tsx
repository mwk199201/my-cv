import styled from "styled-components";
import { Portfolio } from "../type";

const Content = styled.div`
  height: auto;
  width: auto;
  display: flex;
  color: #000;
`;
const Header = styled.div`
  height: auto;
  width: auto;
  display: flex;
  color: #000;
`;

const Section = styled.div`
  width: 90%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
`;

export default function CVContent() {
  return (
    <Section>
      <Header></Header>
      <Content></Content>
    </Section>
  );
}
