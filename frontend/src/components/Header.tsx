import { render } from "@testing-library/react";
import styled from "styled-components";
import myImage from "../img/image.png";

const Header = styled.header`
  background: linear-gradient(#498dfc, #bbf4fc);
  height: 215px;
  width: 100%;
  display: flex;
`;

const HeaderImage = styled.img`
  height: 80px;
  width: 80px;
  margin: 15px;
`;

export default function CVHeader() {
  return (
    <div>
      <Header>
        <HeaderImage src={myImage}></HeaderImage>
      </Header>
    </div>
  );
}
